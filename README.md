[TOC]

**解决方案介绍**
===============
该解决方案能帮助用户快速部署[Yonghong Z-Suite](https://www.yonghongtech.com/cp/suite/)平台，Yonghong Z-Suite是一站式大数据分析平台，全面覆盖数据分析过程中的各个环节，包括数据采集、清洗、整合、存储、计算、建模、训练、展现、协作等，让用户可以在一个统一的平台上完成全流程数据分析任务，极大降低了实施、集成、培训的成本，帮助企业轻松构建数据应用。

解决方案实践详情页面：[https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-athenabi-platform.html](https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-athenabi-platform.html)

**架构图**
---------------
![方案架构](./document/rapid-deployment-of-athenabi-platform.png)

**架构描述**
---------------
该解决方案会部署如下资源：

1、创建一台弹性云服务器 ECS，用于部署Yonghong Z-Suite平台。

2、创建一个弹性公网IP EIP，用于提供服务器访问公网和被公网访问能力。

3、创建安全组，通过配置安全组规则，为弹性云服务器提供安全防护。


**组织结构**
---------------

``` lua
huaweicloud-solution-rapid-deployment-of-athenabi-platform
├── rapid-deployment-of-athenabi-platform.tf.json -- 资源编排模板
```
**开始使用**
---------------
1、访问Yonghong Z-Suite平台。复制图中所看到的访问地址，在浏览器打开。

![创建的云服务器](./document/readme-image-001.png)

2、用户第一次访问会提示需要产品的[license](https://marketplace.huaweicloud.com/contents/74c32078-62d3-475e-a96d-00f1b8554e54)，请您联系永洪科技获取，方可正常使用Yonghong Z-Suite产品，官方文档点击[这里](https://www.yonghongtech.com/zc/wdxz/)查看。

